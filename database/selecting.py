from pymongo import MongoClient
myclient = MongoClient('localhost', 27017)

mydb = myclient['mydatabase']
mycol = mydb['customers']

# Selecting one entry
#print(mycol.find_one())


# To print available fields
# x = mycol.find_one()
# x = dict(x)
# for i in x.keys():
#     print(i)



# Selecting some entry
# x = mycol.find({}, {"_id": 0, "name": 0})
# not allowed to specify both 0 and 1 \
# values in the same object
# for i in x:
#     print(i)

myquery = {'address': "Park Lane 38"}

mydoc = mycol.find(myquery)

for x in mydoc:
    print(x)