from pymongo import MongoClient
myclient = MongoClient('localhost',27017)
mydb = myclient['mydatabase']
mycol = mydb['customers']

myquery = {"address": {"$regex": "^S"}}
#x = mycol.delete_one(myquery)
x = mycol.delete_many(myquery)
for item in mycol.find():
    print(item)

# Deleting All Docs
x = mycol.delete_many({})
print(x.deleted_count, "documents Deleted")