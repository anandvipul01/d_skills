from pymongo import MongoClient
myclient = MongoClient('localhost', 27017)
mydb = myclient['quotes']

print(mydb.list_collection_names())
mycol = mydb['quotes']
print(len(list(mycol.find())))


myclient.drop_database('quotes')
print(myclient.list_database_names())