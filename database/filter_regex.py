from pymongo import MongoClient

myclient = MongoClient('localhost', 27017)
mydb = myclient['mydatabase']
mycol = mydb['customers']

myquery = {'address': {'$regex': '^S'}}

mydoc = mycol.find(myquery)

for x in mydoc:
    print(x)