from pymongo import MongoClient
myclient = MongoClient('localhost', 27017)

mydb = myclient['mydatabase']
mycol = mydb['customers']

# Filter with Modifiers
myquery = {'address': {'$gt':'S'}}
mydoc = mycol.find(myquery)

for x in mydoc:
    print(x)