# Implementing Hashmap in python

class hashmap(object):
    # Definition for init
    def __init__(self, size):
        self.size = size
        self.slot = [None]*self.size
        self.data = [None]*self.size
    # Definition for put
    def put(self, key, val):
        hashval = self.hashfunc(key, len(self.slot))

        if self.slot[hashval] == None:
            self.slot[hashval] = key
            self.data[hashval] = val

        else:
            if self.slot[hashval] == key:
                self.data[hashval] = val
            else:
                nextslot = self.rehash(hashval, len(self.slot) - 1)

                while self.slot[nextslot] != None and self.slot[nextslot] != key:
                    nextslot = self.rehash(nextslot, len(self.slot) - 1)

                if self.slot[nextslot] == None:
                    self.slot[nextslot] = key
                    self.data[nextslot] = val

                else:
                    self.data[nextslot] = val
     # Definition for hashfunc
    def hashfunc(self, key, size):
        return key%self.size
    # Definition for rehash
    def rehash(self, oldsize, size):
        return (oldsize+1)%size
    # Definition for get
    def get(self, key):
        startpos = self.hashfunc(key, len(self.slot))
        pos = startpos
        data = None

        found = False
        stop = False

        while self.slot[pos] != None and not found and not stop:
            if self.slot[pos] == key:
                found = True
                data = self.data[pos]

            else:
                pos = self.rehash(pos, len(self.slot))
                if pos == startpos:
                    stop = True
        return data

    # Operator Overloading for getitem and setitem
    def __getitem__(self, key):
        return self.get(key)
    def __setitem__(self, key, val):
        return self.put(key, val)


if __name__ == '__main__':
    a = hashmap(4)
    a[4] = 'vipul'
    print(a[4])
    a[4] = 'test'
    print(a[4])
    