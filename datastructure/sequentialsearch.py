def seq_search(arr, key):
    found = False
    curr_pos = 0

    while not found and curr_pos < len(arr):
        if arr[curr_pos] == key:
            found = True
        else:
            curr_pos += 1

    return found

# If the list is ordered lets ay in asscending order then 
# if key is less than the 0th element then the item is not present


def ordered_seq_search(arr, key):
    stop = False
    found = False
    pos = 0

    while pos < len(arr) and not found and not stop:
        if arr[pos] == key:
            found = True
        else:
            if arr[pos] > key: # This states that no smaller element is present top get comared with key
                print('Stopped = True')
                stop = True
            else:
                pos += 1

    return found


if __name__ == "__main__":
    a = [3,4,5,7,87,9]
    print(seq_search(a,7))
    a = [3,4,5,7,87,9]
    print(seq_search(a,80))
    print("---------ordered_seq_search----------------")
    a.sort()
    print(ordered_seq_search(a,2))