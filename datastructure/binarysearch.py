def binsearch(arr, key):
    a = len(arr)
    first = 0
    last = len(arr) - 1
    mid = (first + last)//2

    arr.sort()
    if a == 0:
        return False
    else:
        if arr[mid] == key:
            return True
        else:
            if key > arr[mid]:
                return binsearch(arr[mid+1:], key)
            else:
                return binsearch(arr[:mid], key)

if __name__ == "__main__":
    ar = [1,2,3,8,4,5]
    print(binsearch(ar,5))