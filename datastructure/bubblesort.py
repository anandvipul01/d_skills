def bub_sort(arr):
    for i in range(len(arr)-1, 0, -1):
        for j in range(i):
            if arr[j] > arr[j+1]:
                temp = arr[j]
                arr[j] = arr[j+1]
                arr[j+1] = temp

if __name__ == '__main__':
    array_in = [4,6,8,4,2,54,8,9]
    print(array_in)
    bub_sort(array_in)
    print("After Sorting \n",array_in) 
    