def insertion_sort(arr):
    for i in range(1,len(arr)):
        currentvalue = arr[i]
        pos = i

        while pos > 0 and arr[pos - 1] > currentvalue:
            arr[pos] = arr[pos - 1]
            pos = pos -1
            print(arr)
        arr[pos] = currentvalue


if __name__ == "__main__":
    ar = [2,4,6,5,3,9,6,5]
    insertion_sort(ar)

    print(ar)