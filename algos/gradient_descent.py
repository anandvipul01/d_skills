x = [1, 2, 4, 3, 5]
y = [1, 3, 3, 2, 5]

alpha = 0.01
b0 = 0
b1 = 0
def func_calc(b0, b1, x):
    return b0 + b1*x

def batch_grad():
    global b0, b1, x, y
    for i in range(len(x)):
        error = func_calc(b0, b1, x[i]) - y[i]
        # Update
        b0 = b0-alpha*error
        b1 = b1-alpha*error*x[i]
        print(round(b0,5), round(b1,5))

for i in range(20):
    batch_grad()