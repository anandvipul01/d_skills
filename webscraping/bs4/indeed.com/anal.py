
# coding: utf-8

# ## Indeed.com [australia]
# #### au.indeed.com

# https://au.indeed.com/jobs-in-Australia

# Title - <h3 class="icl-u-xs-mb--xs icl-u-xs-mt--none  jobsearch-JobInfoHeader-title">Christmas Casuals</h3>
# xpath = "//h3[@class = 'icl-u-xs-mb--xs icl-u-xs-mt--none  jobsearch-JobInfoHeader-title']/text()"
# 
# 
# Company Name = <h4 class="jobsearch-CompanyReview--heading">FIS Global</h4>
# xpath = "//h4[@class = 'jobsearch-CompanyReview--heading']/text()"
# 
# Company Name = <div class="icl-u-lg-mr--sm icl-u-xs-mr--xs">Amber Road India</div>
# xpath = "//div[@class = 'icl-u-lg-mr--sm icl-u-xs-mr--xs']/descendant-or-self::*/text()"
# 
# 
# 
# Company Rating = <meta itemprop="ratingValue" content="4.099999904632568">
# xpath = "//meta[@itemprop = 'ratingValue']/@content"
# 
# Company Review = <meta itemprop="ratingCount" content="8321">
# xpath = "//meta[@itemprop = 'ratingCount']/@content"
# 
# 
# Job Location = <span class="jobsearch-JobMetadataHeader-iconLabel">New South Wales</span>
# xpath = "//span[@class = 'jobsearch-JobMetadataHeader-iconLabel']/text()"
# 
# 
# 
# Job Description = <div id="jobDescriptionText" class="jobsearch-jobDescriptionText">
# xpath = "//div[@id = 'jobDescriptionText' and @class = 'jobsearch-jobDescriptionText']/descendant-or-self::*/text()".getall()
# 
# 
# 
# 
# 
# <div class="jobsearch-JobMetadataFooter"><span class="icl-u-textColor--success">Woolworths Group</span> - Today<span id="saveJobInlineCallout" class="icl-u-lg-inline icl-u-xs-hide"> - <a title="Save this job to my.indeed">save job</a></span><div id="mosaic-provider-reportcontent" class="mosaic mosaic-provider-reportcontent"><span><span class="mosaic-reportcontent-link"><a type="link">report job</a></span><div class="mosaic-reportcontent-content"></div></span></div><span id="originalJobLinkContainer" class="icl-u-lg-inline icl-us-xs-hide"> - <a target="_blank" rel="noopener" href="https://au.indeed.com/rc/clk?jk=8bc16b0870f52883&amp;from=vj&amp;pos=top">original job</a></span><div id="saveJobInlineCalloutContainer" class="icl-u-lg-block icl-u-xs-hide icl-u-lg-mt--md"></div></div>

# In[1]:


import pandas as pd
from bs4 import BeautifulSoup
import requests
import time
from datetime import datetime
import progressbar
import json


# In[2]:


now = datetime.now()
tim = now.strftime("%d/%m/%Y").replace('/','_' )

# with open('job_csv'+tim+'.csv', 'a+') as f:
#     f.write(','.join(['Company_Name','Company_Rating','Company_Reviews','Job_Location','Date_of_Posting[Referenced]','Job_Description','Crawl_Time','Time_Zone']))
#     f.write('\n')


# In[3]:


N = 100
lis_indeed = []
lis_url = []
lis_time = []
lis_all = []
lis_title = []
lis_link = []
def writer_func(i, temp, url, title, tim, job_link):
    with open('file_'+str(i)+'.html', 'w+') as f:
        f.write(temp+'\n')
        lis_indeed.append(temp)
        for i in range(10):
            lis_title.append(title[i])
            lis_link.append(job_link[i])
        lis_all.append((temp,url,tim))
        lis_time.append(tim)
        lis_url.append(url)


# In[4]:


company_name_list = []
company_rating_list = []
company_reviews_list = []
job_location_list = []
job_description_list = []
date_posting_list = []
crawl_time_list = []
time_zone_list = []
#--------------------------------------
job_title = []
company_name_main = []
company_link_main = []
company_rating_main = []
company_reviews_main = []
job_location_main = []
job_summary_main = []
date_posting_main = []
#--------------------------------------
def data(url):
    temp = requests.get(url).text
    html_soup = BeautifulSoup(temp)
    a = html_soup.find_all('div', class_ = 'jobsearch-SerpJobCard unifiedRow row result')
    job_link = []
    for i in range(10):
        now = datetime.now()
        crawl_time = now.strftime("%d/%m/%Y %H:%M:%S")
        time_zone_list.append(time.tzname[0])
        crawl_time_list.append(crawl_time)
        
        b = a[i].find_all('div', class_ = 'title')
        try:
            job_title.append(str(b[0].a.text).strip())
        except:
            job_title.append('')
        try:
            company_name_main.append(a[i].find_all('div', class_ = "sjcl")[0].find('span', class_ = 'company').a.text)
        except:
            company_name_main.append('')
        try:
            company_link_main.append(a[i].find_all('div', class_ = "sjcl")[0].find('span', class_ = 'company').a['href'])
        except:
            company_link_main.append('')
        try:
            job_location_main.append(list(a[i].find_all('span', class_ = "location accessible-contrast-color-location"))[0].text)
        except:
            job_location_main.append('')
        try:
            job_summary_main.append(list(a[i].find_all('div', class_ = "sjcl")[0].next_siblings)[1].text)
        except:
            job_summary_main.append('')
        try:
            date_posting_main.append(a[i].find_all('span', class_ = 'date')[0].text)
        except:
            date_posting_main.append('')
        
        #company_rating_main.append(str(a[i].find_all('div', class_ = "sjcl")[0].find_all('span', class_ = 'ratingsContent')[0].text))
        #company_reviews_main.append(list(a[i].find_all('div', class_ = "sjcl")[0].find('span', class_ = 'ratings').next_siblings)[1].text)
        
        
        
        job_link.append('https://au.indeed.com'+b[0].a['href'])
        job_url_1 = 'https://au.indeed.com'+b[0].a['href']
        #time.sleep(5)
        #-----------------------------------------------------------
        obj = extr_data(job_url_1)
#         company_name_list.append(obj['Company_Name'])
        company_rating_list.append(obj['Company_Rating'])
        company_reviews_list.append(obj['Company_Reviews'])
#         job_location_list.append(obj['Job_Location'])
        job_description_list.append(obj['Job_Description'])
#         date_posting_list.append(obj['Date_of_Posting[Referenced]'])
#         crawl_time_list.append(obj['Crawl_Time'])
#         time_zone_list.append(obj['Time_Zone'])
        #-----------------------------------------------------------
#         print("Scraping URL: "+url+"\n")
#         write_json(obj)
    return temp, job_title, job_link 


# In[5]:


def extr_data(url):
    temp = requests.get(url).text
    time.sleep(5)
    html_soup = BeautifulSoup(temp)
    now = datetime.now()
    try:
#         company_name = html_soup.find_all('div', class_ = 'icl-u-lg-mr--sm icl-u-xs-mr--xs')[0].text
        company_rating = html_soup.find_all('meta', itemprop = 'ratingValue')[0]['content']
        company_reviews = html_soup.find_all('meta', itemprop = 'ratingCount')[0]['content']
#         job_location = html_soup.find_all('span', class_ = 'jobsearch-JobMetadataHeader-iconLabel')[0].text
        job_description = html_soup.find_all('div', class_ = 'jobsearch-jobDescriptionText' ,id = 'jobDescriptionText')[0].text
#         date_posting = html_soup.find_all('div', class_ = 'jobsearch-JobMetadataFooter')[0].text.split('-')[1]
#         crawl_time = now.strftime("%d/%m/%Y %H:%M:%S")
#         time_zone = time.tzname[0]
    except:
        return {
#         "Company_Name": '',
        "Company_Rating": '',
        "Company_Reviews": '',
#         "Job_Location": '',
#         "Date_of_Posting[Referenced]": '',
        "Job_Description": ''
#         "Crawl_Time": now.strftime("%d/%m/%Y %H:%M:%S"),
#         "Time_Zone": time.tzname[0]
    }
    data_job = {
#         "Company_Name": company_name,
        "Company_Rating": company_rating,
        "Company_Reviews": company_reviews,
#         "Job_Location": job_location,
#         "Date_of_Posting[Referenced]": date_posting,
        "Job_Description": job_description
#         "Crawl_Time": crawl_time,
#         "Time_Zone": time_zone
    }
    print('\n\n')
    return data_job


# In[6]:


for i in range(N):
    time.sleep(2)
    if i == 0:
        base_url = "https://au.indeed.com/jobs?q=&l=Australia"
        temp, title, job_link = data(base_url)
#         now = datetime.now()
#         tim = now.strftime("%d/%m/%Y %H:%M:%S")
#         writer_func(i,temp, base_url, title, tim, job_link)        
    else:
        url = base_url + '&start=' + str(i*10)
        temp, title, job_link = data(url)
        print("Scrapping URL: ", url)
#         now = datetime.now()
#         tim = now.strftime("%d/%m/%Y %H:%M:%S")
#         writer_func(i,temp, url, title, tim, job_link) 
        


# In[13]:


data2 = pd.DataFrame({
        "Job_Title": job_title,
        "Company_Name": company_name_main,
        "Company_Link": company_link_main,
        "Company_Rating": company_rating_list,
        "Company_Reviews": company_reviews_list,
        "Job_Location": job_location_main,
        "Time_Zone": time_zone_list,
        "Crawl_Time": crawl_time_list,
        "Date_of_Posting[Referenced]": date_posting_main,
        "Job_Summary": job_summary_main,
        "Job_Description": job_description_list        
    })
data2.to_csv('data.csv')


# In[17]:


aaaa = pd.read_csv('data.csv')


# In[19]:


print(aaaa.shape)

