
import scrapy
from scrapy.loader import ItemLoader
from tutorial.items import QuoteItem


class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    page_count = 0
    def start_requests(self):
        urls = ['http://quotes.toscrape.com/'
        ]
        for url in urls:
            yield scrapy.Request(url = url, callback = self.parse)

    def parse(self, response):
        # page = response.url.split('/')[-2]
        # filename = 'quotes-%s.html'%page
        # with open(filename, 'wb') as f:
        #     f.write(response.body)
        # self.log('Saved File %s' % filename)
        
        self.page_count += 1
        quotes = response.xpath('//div[@class="quote"]')
        for quote in quotes:
            loader = ItemLoader(item = QuoteItem(), selector = quote, response = response)
            loader.add_xpath('text', './/span[@class="text"]/text()')
            loader.add_xpath('author', './/small[@class="author"]')
            loader.add_xpath('tags', './/a[@class="tag"]')
            yield loader.load_item()
            # text = quote.xpath('.//span[@class="text"]/text()').get()
            # author = quote.xpath('.//small[@class="author"]/text()').get()
            # tags = quote.xpath('.//a[@class="tag"]/text()').extract()

            # with open('quote.txt', 'a+') as f:
            #     f.write('---------'+str(self.page_count)+'-------\n')
            #     f.write(text)
            #     f.write('\n\n\n')
            #     f.write(author)
            #     f.write('\n\n\n')
            #     f.write(str(tags))
            #     f.write('\n\n\n')
            # yield {
            #     'text' : text,
            #     'author' : author,
            #     'tags' : tags
            # }
        next_page = response.xpath('//li[@class="next"]/a/@href').get()
        if next_page is not None:
            next_page_url = response.urljoin(next_page)
            yield scrapy.Request(url = next_page_url, callback = self.parse)
