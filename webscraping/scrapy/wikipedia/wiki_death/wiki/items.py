# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst, Join
from w3lib.html import remove_tags
import datetime


def name_bio_edit(value):
    return value[4:]
def age_edit(value):
    try:
        return int(value[:2])
    except:
        return value
def month_edit(value):
    return value[1:-1]


class WikiItem(scrapy.Item):

    name = scrapy.Field(
        input_processor = MapCompose(remove_tags),
        output_processor = TakeFirst()
    )
    name_bio = scrapy.Field(
        input_processor = MapCompose(name_bio_edit, remove_tags),
        output_processor = TakeFirst()
    )
    year_death = scrapy.Field(
        input_processor = MapCompose(),
        output_processor = TakeFirst()
    )
    month_death = scrapy.Field(
        input_processor = MapCompose(month_edit),
        output_processor = TakeFirst()
    )
    age = scrapy.Field(
        input_processor = MapCompose(age_edit),
        output_processor = TakeFirst()
        
    )
    date = scrapy.Field(
        input_processor = MapCompose(),
        output_processor = TakeFirst()
    )
