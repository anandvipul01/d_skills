import scrapy
import time
from scrapy.loader import ItemLoader
from wiki.items import WikiItem

class WikibotSpider(scrapy.Spider):
    # Name of the spider
    name = 'wikispider'

    def start_requests(self):
        start_url = 'https://en.wikipedia.org/wiki/Lists_of_deaths_by_year'

        yield scrapy.Request(url = start_url, callback = self.parse)

    def parse(self, response):
        count = 0
        year_selector = response.xpath("//h2/span[@class = 'mw-headline']/text()").getall()
        for year in year_selector:
            count += 1
            number_of_months = response.xpath("//div[@class = 'div-col columns column-width']["+str(count)+"]/ul/li")
            for month in number_of_months:
                base_url = "https://en.wikipedia.org"
                get_url = month.xpath("./a/@href").get()
                forward_url = base_url + get_url
                time.sleep(5)
                yield scrapy.Request(url = forward_url, callback = self.parser2)

    def parser2(self, response):
        number_of_days = len(response.xpath("//td[@colspan = '3' and @style = 'padding-left:0.5em; padding-right:0.5em']/ul/li"))
        for i in range(number_of_days):
            number_of_death = len(response.xpath("//h2/following::h3["+str(i+1)+"]/following-sibling::ul["+str(1)+"]/child::li"))
            for j in range(number_of_death):
                name = response.xpath("//h2/following::h3["+str(i+1)+"]/following-sibling::ul["+str(1)+"]/" +"li["+str(j+1)+"]/a/text()").get()
                name_demo = response.xpath("//h2/following::h3["+str(i+1)+"]/following-sibling::ul["+str(1)+"]/" +"li["+str(j+1)+"]/text()").get()
                try:
                    age = name_demo.split()[1:-1][0]
                    name_demo = ' '.join(name_demo.split()[1:-1])
                    
                except:
                    continue
                year_death = response.url[-4:]
                month_death = response.url[39:-4]
                loader = ItemLoader(item = WikiItem(), response = response)
                loader.add_value('name', name)
                loader.add_value('name_bio',name_demo)
                loader.add_value('year_death', year_death)
                loader.add_value('month_death', month_death)
                loader.add_value('age', age)
                loader.add_value('date', str(i+1) + "-" + month_death[1:-1]+ "-" + str(year_death))

                yield loader.load_item()

                


