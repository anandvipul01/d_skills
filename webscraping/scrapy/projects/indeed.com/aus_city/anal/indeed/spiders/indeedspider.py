import scrapy
from scrapy_splash import SplashRequest

from datetime import datetime
class IndeedSpider(scrapy.Spider):
    name = 'indeedspider'

    def start_requests(self):
        # start_url = 'https://cn.indeed.com/工作-地点-China'
        url_list = ['https://au.indeed.com/jobs?q=&l=Northern+Territory', \
            'https://au.indeed.com/jobs?q=&l=Tasmania', \
                'https://au.indeed.com/jobs?q=&l=Western+Australia', \
                    'https://au.indeed.com/jobs?q=&l=South+Australia', \
                        'https://au.indeed.com/jobs?q=&l=Queensland', \
                            'https://au.indeed.com/jobs?q=&l=Victoria', \
                                'https://au.indeed.com/jobs?q=&l=New+South+Wales', \
                                    'https://au.indeed.com/jobs?q=&l=Australian+Capital+Territory']
        for item in url_list:
            yield SplashRequest(url = item, callback=self.parse, endpoint = 'render.html', args = {'wait':2})

    def parse(self, response):
        a = response.xpath("//div[@class = 'jobsearch-SerpJobCard unifiedRow row result clickcard']/div[@class = 'title']/a")
        pages = response.xpath("//div[@class = 'pagination']/a/@href").getall()
        temp_url_base = response.url.split('/')[2]
        for i in a:
            next_url = "https://" + temp_url_base + str(i.xpath('./@href').get())
            yield SplashRequest(url = next_url, callback=self.parse_2, endpoint = 'render.html', args = {'wait':2})
            with open('vipul.txt', 'a+') as f:
                f.write(next_url)
                f.write('\n')
        next_page = pages[-1]
        if next_page is not None:
            next_page_url = "https://" + temp_url_base + next_page
            with open('vipul.txt', 'a+') as f:
                f.write(next_page_url)
                f.write('\n')
            yield SplashRequest(url = next_page_url, callback=self.parse, endpoint = 'render.html', args = {'wait':2})
        
    def parse_2(self, response):
        # Extracting Job Title
        try:
            job_title = response.xpath("//h3[@class = 'icl-u-xs-mb--xs icl-u-xs-mt--none  jobsearch-JobInfoHeader-title']/text()").get()
        except:
            job_title = 'None'
        # Extracting Company Name
        try:
            company_name = response.xpath("//div[@class = 'icl-u-lg-mr--sm icl-u-xs-mr--xs']/descendant-or-self::*/text()").get()
        except:
            company_name = 'None'
        # Extracting Company Rating
        try:
            company_rating = response.xpath("//meta[@itemprop = 'ratingValue']/@content").get()
        except:
            company_rating = 'None'
        # Extracting Company Review
        try:
            company_review = response.xpath("//meta[@itemprop = 'ratingCount']/@content").get()
        except:
            company_review = 'None'
        # Extracting Job Location
        try:
            job_location = response.xpath("//span[@class = 'jobsearch-JobMetadataHeader-iconLabel']/text()").get()
        except:
            job_location = 'None'
        # Extracting Job Description
        try:
            job_description = response.xpath("//div[@id = 'jobDescriptionText' and @class = 'jobsearch-jobDescriptionText']/descendant-or-self::*/text()").getall()
            job_description = ''.join(job_description)
        except:
            job_description = 'None'
        # Extracting Date Posted
        try:
            date_posted = response.xpath("//div[@class = 'jobsearch-JobMetadataFooter']/text()").get().split('-')[-1]
        except:
            date_posted = ''
        # Setting Crawl Time stamp
        crawl_timestamp = datetime.now()

        yield {
            'Job_Title': job_title,
            'Company_Name': company_name,
            'Company_Rating': company_rating,
            'No. of Reviews': company_review,
            'Job_Location': job_location,
            'Job_Description': job_description,
            'date_posted': date_posted,
            'Crawl_TimeStamp': crawl_timestamp
        }
