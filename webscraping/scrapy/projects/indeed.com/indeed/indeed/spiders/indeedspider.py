import scrapy
from scrapy_splash import SplashRequest
class IndeedSpider(scrapy.Spider):
    name = 'indeedspider'

    def start_requests(self):
        start_url = 'https://au.indeed.com/jobs-in-Australia'
        yield SplashRequest(url = start_url, callback=self.parse, endpoint = 'render.html', args = {'wait':2})

    def parse(self, response):
        a = response.xpath("//div[@class = 'jobsearch-SerpJobCard unifiedRow row result clickcard']/div[@class = 'title']/a")
        pages = response.xpath("//div[@class = 'pagination']/a/@href").getall()
        
        for i in a:
            next_url = "https://au.indeed.com" + str(i.xpath('./@href').get())
            yield SplashRequest(url = next_url, callback=self.parse_2, endpoint = 'render.html', args = {'wait':2})
            with open('vipul.txt', 'a+') as f:
                f.write(next_url)
                f.write('\n')
        next_page = pages[-1]
        if next_page is not None:
            next_page_url = "https://au.indeed.com" + next_page
            with open('vipul.txt', 'a+') as f:
                f.write(next_page_url)
                f.write('\n')
            yield SplashRequest(url = next_page_url, callback=self.parse, endpoint = 'render.html', args = {'wait':2})
        
    def parse_2(self, response):
        company_name = response.xpath("//div[@class = 'icl-u-lg-mr--sm icl-u-xs-mr--xs']/descendant-or-self::*/text()").get()
        company_rating = response.xpath("//meta[@itemprop = 'ratingValue']/@content").get()
        company_review = response.xpath("//meta[@itemprop = 'ratingCount']/@content").get()
        job_location = response.xpath("//span[@class = 'jobsearch-JobMetadataHeader-iconLabel']/text()").get()
        job_description = response.xpath("//div[@id = 'jobDescriptionText' and @class = 'jobsearch-jobDescriptionText']/descendant-or-self::*/text()").getall()

        yield {
            'Company_Name': company_name,
            'Company_Rating': company_rating,
            'No. of Reviews': company_review,
            'Job_Location': job_location,
            'Job_Description': str(job_description)
        }
