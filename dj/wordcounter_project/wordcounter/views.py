from django.http import HttpResponse
from django.shortcuts import render
import operator

def home(request):
    return render(request, 'home.html')

def count(request):
    fulltext = text = request.GET['fulltext']
    # print(fulltext)
    wordlist = fulltext.split()
    word_dict = {}
    for i in wordlist:
        if i in word_dict:
            word_dict[i] += 1
        else:
            word_dict[i] = 1
    word_dict = sorted(word_dict.items(), key = operator.itemgetter(1), reverse = True)
    return render(request, 'count.html', {'fulltext': fulltext, 'count': len(wordlist), 'word_dict':word_dict})